<?php

// 60*60*24
define('SECONDS_TO_DAYS', 86400);

function getServices()
{
    $output = shell_exec('avahi-browse -p -k _ultimaker._tcp -t -r');
    $ret = array();
    foreach(explode("\n", $output) as $line)
    {
        $data = str_getcsv($line, ";");
        if (count($data) < 10)
            continue;
        if ($data[0] != "=")
            continue;
        if ($data[2] != "IPv4")
            continue;
        $obj = new stdClass();
	$obj->name = $data[3];
        $obj->number = hexdec(substr($obj->name, -6));
	$obj->ip = $data[7];
        $obj->port = $data[8];
        $obj->data = array();
        foreach(str_getcsv($data[9], " ") as $key_value)
        {
            $key_value = explode("=", $key_value, 2);
            if (count($key_value) != 2)
                continue;
            $obj->data[$key_value[0]] = $key_value[1];
        }
        $obj->data["hotend_time_hot_0"] = getTimeHot($obj->ip, 0);
        $obj->data["hotend_time_hot_1"] = getTimeHot($obj->ip, 1);
        $ret[] = $obj;
    }
    return $ret;
}

function getTimeHot($ip, $hotend_index)
{
    $url = "http://${ip}/api/v1/printer/heads/0/extruders/${hotend_index}/hotend/statistics/time_spent_hot";
    // create the context
    $arContext['http']['timeout'] = 1;
    $context = stream_context_create($arContext);

    try {
        $seconds = file_get_contents($url, 0, $context);
        return $seconds / SECONDS_TO_DAYS;
   } 
   catch (Exception $e) {
       return 0;
   }
}

echo  json_encode(getServices());

?>
